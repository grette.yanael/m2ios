//
//  ViewController.swift
//  TableTest
//
//  Created by tp on 12/02/2020.
//  Copyright © 2020 ui. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITableViewDataSource {
    
    var data = ["Look at the move", "Look at the cleans", " FAKER", "WHAT WAS THAT ?!"]
    
    
    @IBOutlet weak var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellule = tableView.dequeueReusableCell(withIdentifier: "cellule", for: indexPath)
        cellule.textLabel?.text = data[indexPath.row]
        return cellule
    }
    
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.dataSource = self
    }


}

