//
//  ViewController.swift
//  mapTest
//
//  Created by Ali ED-DBALI on 25/03/2018.
//  Copyright © 2018 Ali ED-DBALI. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController {

    @IBOutlet weak var map: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // 47.845665, 1.939773 = Département d'informatique
        
        // Placer une région dans la carte
        
        let latitude:CLLocationDegrees = 47.845665
        
        let longitude:CLLocationDegrees = 1.939773
        
        let latDelta:CLLocationDegrees = 0.01
        
        let lonDelta:CLLocationDegrees = 0.01
        
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        
        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        
        map.setRegion(region, animated: true)
        
        
        // Placer l'annotation
        
        let annotation = MKPointAnnotation()
        
        annotation.coordinate = location
        
        annotation.title = "Département d'informatique"
        
        annotation.subtitle = "J'y suis tout le temps..."
        
        map.addAnnotation(annotation)
        
        // let actionAnnotation = UILongPressGestureRecognizer(target: self, action: #selector(detail))
        
        let uilpgr = UILongPressGestureRecognizer(target: self, action: #selector(action))
        
        uilpgr.minimumPressDuration = 2
        
        map.addGestureRecognizer(uilpgr)
        // map.addGestureRecognizer(actionAnnotation)
        
        
    }
    
    @objc func detail(gestureRecognizer: UIGestureRecognizer) {
        
    }
    
    @objc func action(gestureRecognizer: UIGestureRecognizer) {
        
        print("Le geste a été reconnu !")
        
        let touchPoint = gestureRecognizer.location(in: self.map)
        
        let newCoordinate: CLLocationCoordinate2D = map.convert(touchPoint, toCoordinateFrom: self.map)
        
        let annotation = MKPointAnnotation()
        
        annotation.coordinate = newCoordinate
        
        annotation.title = "Nouvel emplacement"
        
        annotation.subtitle = "Un jour j'irai ici..."
        
        map.addAnnotation(annotation)
        
        
        
    }
    

}

