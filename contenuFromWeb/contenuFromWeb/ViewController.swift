//
//  ViewController.swift
//  contenuFromWeb
//
//  Created by Ali ED-DBALI on 10/03/2018.
//  Copyright © 2018 Ali ED-DBALI. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var categorie = [String]()
    
    var data = JSON()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        
        let url = URL(string: "http://localhost:8888/data/monuments.json")
        
        Alamofire.request(url!, method: .get).responseJSON { (reponse) in
            
            if reponse.result.isSuccess {
                let contenuJSON : JSON = JSON(reponse.result.value!)
                self.data = contenuJSON
                self.categorie = self.lesTitres(self.data)
                self.tableView.reloadData()
                // print(contenuJSON)
            } else {
                print("Erreur : \(reponse.result.error!)")
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return categorie.count
        
    }
  
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellule = tableView.dequeueReusableCell(withIdentifier: "MaCellule", for: indexPath)
        
        cellule.textLabel?.text = categorie[indexPath.row]
        
        return cellule
        
    }

    private func lesTitres(_ data : JSON) -> [String] {
        
        var titre = [String]()
        
        for elt in data["categories"].arrayValue {
            titre.append(elt["titre"].stringValue)
        }
        return titre
    }

}







