//
//  ViewController.swift
//  chrono
//
//  Created by tp on 31/01/2020.
//  Copyright © 2020 tp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var chrono : Int = 0;
    var timer = Timer()
    
    @IBOutlet weak var temps: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        temps.text = "\(chrono/3600%24):\(chrono/60%60):\(chrono%60)"
    }

    @IBAction func start(_ sender: UIBarButtonItem) {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector:#selector(self.incrementer), userInfo: nil, repeats: true)
    }
    
    @IBAction func pause(_ sender: UIBarButtonItem) {
        pause_chrono()
    }
    
    @IBAction func stop(_ sender: UIBarButtonItem) {
        stop_chrono()
    }
    
    
    @objc func incrementer() {
        chrono += 14484;
        temps.text = "\(chrono/3600%24):\(chrono/60%60):\(chrono%60)"
    }
    
    @objc func pause_chrono() {
        timer.invalidate();
    }
    
    @objc func stop_chrono() {				
        timer.invalidate()
        chrono = 0
        temps.text = "\(chrono/3600%24):\(chrono/60%60):\(chrono%60)"
    }
}

