//
//  ViewController.swift
//  tic-tac-toc
//
//  Created by tp on 20/01/2020.
//  Copyright © 2020 tp. All rights reserved.
//
//Creer UNE FONCTION PAR BOUTON qui va vérifier si le bouton a déjà été cliqué ou non
//appeler la fonction commune pour l'affichage
//utiliser une liste de liste
import UIKit

class ViewController: UIViewController {

    
    var joueur = 1
    @IBOutlet weak var btn11: UIButton!
    
    @IBOutlet weak var btn12: UIButton!
    
    @IBOutlet weak var btn13: UIButton!
    
    @IBOutlet weak var btn21: UIButton!
    
    @IBOutlet weak var btn22: UIButton!
    
    @IBOutlet weak var btn23: UIButton!
    
    @IBOutlet weak var btn31: UIButton!
    
    @IBOutlet weak var btn32: UIButton!
    
    @IBOutlet weak var btn33: UIButton!
    
    
    
    @IBAction func clickedButton(_ sender: AnyObject) {
        print("NOOTNOOT")
        if sender.image != nil {
            print("DEJA FAIT")
            return
        }
        if joueur == 1  {
            joueur = 2
            sender.setImage(UIImage(named: "rond.png"), for: .normal)
            
        }
        else {
            joueur = 1
            sender.setImage(UIImage(named: "croix.png"), for: .normal)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    

    @IBAction func again(_ sender: Any) {
        joueur = 1
        btn11.setImage(UIImage(), for: .normal)
        btn12.setImage(UIImage(), for: .normal)
        btn13.setImage(UIImage(), for: .normal)
        btn21.setImage(UIImage(), for: .normal)
        btn22.setImage(UIImage(), for: .normal)
        btn23.setImage(UIImage(), for: .normal)
        btn31.setImage(UIImage(), for: .normal)
        btn32.setImage(UIImage(), for: .normal)
        btn33.setImage(UIImage(), for: .normal)
    }
}

